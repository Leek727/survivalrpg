"""BE CAREFUL WHEN RUNNING THIS FILE. EVERYTHING WILL BE RESET BACK TO DEFAULT"""
import pickle
import json
import sys

if __name__ == "__main__":

    if (
        input(
            'Are you sure you want to reset world? ["y" for yes, any other for no] >> '
        )
        == "y"
    ):
        pass

    else:
        sys.exit("Aborted procces")

    with open("defaults.json", "r") as default:
        defaults = json.load(default)

    with open("front_assets/assets/worlds/export/items.pkl", "wb") as items:
        pickle.dump(defaults["items"], items)

    with open("front_assets/assets/worlds/export/character.pkl", "wb") as f:
        pickle.dump(defaults["character"], f)

    with open("front_assets/assets/worlds/export/enemies.pkl", "wb") as f:
        pickle.dump(defaults["enemies"], f)

    print("Done. World 'export' has been reset")
