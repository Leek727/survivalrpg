"""
A place to put files to export from the level editor

(currently) takes in
- world dict - 2D array of ID's
- Tile IDs


(currently) outputs
- PNG of background for each chunk

"""

import pygame as pg
import rendering
import pickle as pkl
from os import mkdir


def get_colliders(array2d: list, tile_ids: list, tile_size: int) -> list:
    return optimized_colliders(array2d, tile_ids, tile_size)
    # old code - here if we need it
    colliders = []
    for y, row in enumerate(array2d):
        for x, square in enumerate(row):
            if square:
                if tile_ids[square].is_collideable:
                    rect = pg.Rect(x * tile_size, y * tile_size, tile_size, tile_size)
                    colliders.append(rect)
    return colliders


def optimized_colliders(array2d: list, tile_ids: list, tile_size: int) -> list:
    max_y, max_x = len(array2d), len(array2d[0])
    colliders = [list() for y in range(max_y)]
    simple_colliders = []

    current_y = 0
    start_x, start_y = None, None
    current_rect_width = 0
    previous_collision = False

    while current_y < max_y:
        current_x = 0
        while current_x < max_x:

            # Get the current collision state
            current_ID = array2d[current_y][current_x]
            if current_ID == 0:
                current_collision = False
            else:
                current_collision = tile_ids[current_ID].is_collideable

            if previous_collision:
                if current_collision:
                    # extend current rect
                    current_rect_width += tile_size
                else:
                    # finalize current rect and append it to row's list
                    current_collider = (
                        start_x,
                        current_y * tile_size,
                        current_rect_width,
                        30,
                    )
                    # colliders[current_y].append(current_collider)
                    simple_colliders.append(pg.Rect(current_collider))
            else:
                if current_collision:
                    # start a new rect
                    start_x = current_x * tile_size
                    current_rect_width = tile_size

            previous_collision = current_collision
            current_x += 1
        current_y += 1

    # combine them biatches
    for collider in simple_colliders:
        for i, collider2 in enumerate(simple_colliders):
            if collider != collider2:
                if (
                    (collider.left == collider2.left)
                    and (collider.right == collider2.right)
                    and (
                        (collider.bottom == collider2.top)
                        or (collider2.bottom == collider.top)
                    )
                ):
                    simple_colliders.pop(i)
                    collider.top = max([collider.top, collider2.top])
                    break
    return simple_colliders


def export_world(world: dict, tile_ids: list, tile_size: int) -> None:
    chunks = world["chunks"]
    colliders = dict()

    for name, chunk in chunks.items():

        # create a surface for the image
        height = len(chunk) * tile_size
        width = len(chunk[0]) * tile_size
        background_image = pg.Surface((width, height))

        # render the world to an surface
        rendering.render_screen(background_image, chunk, tile_ids)

        # generate the path to the image
        image_name = f"{name}_background.png"
        image_path = "export/backgrounds/" + image_name

        collider_list = get_colliders(chunk, tile_ids, tile_size)
        colliders[name] = collider_list

        # save surface as a png
        pg.image.save(background_image, image_path)

    with open("export/colliders.pkl", "wb") as file:
        pkl.dump(colliders, file, pkl.HIGHEST_PROTOCOL)
