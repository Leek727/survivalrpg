"""
Spritesheet class to help manage image files
"""

import pygame as pg

# Source : https://www.pygame.org/wiki/Spritesheet


class Spritesheet:
    """
    This class acts as a way to process a spritesheet image into
    it's various sprites.
    """

    def __init__(self, image):
        self.sheet = image
        self.image_width = image.get_width()
        self.image_height = image.get_height()

    # Load a specific image from a specific rectangle
    def image_at(self, rectangle, colorkey=None):

        rect = pg.Rect(rectangle)
        image = pg.Surface(rect.size)
        image.blit(self.sheet, (0, 0), rect)

        if colorkey is not None:
            if colorkey == -1:
                colorkey = image.get_at((0, 0))
            image.set_colorkey(colorkey, pg.RLEACCEL)

        return image

    # Load a whole bunch of images and return them as a list
    def images_at(self, rects, colorkey=None):

        # Loads multiple images, supply a list of coordinates
        return [self.image_at(rect, colorkey) for rect in rects]

    # Load a whole strip of images
    def load_images(self, tile_size, colorkey=None):
        tiles_across = self.image_width // tile_size
        tiles_down = self.image_height // tile_size
        images = []

        for nth_down in range(tiles_down):

            for nth_across in range(tiles_across):

                start_x = tile_size * nth_across
                start_y = tile_size * nth_down

                sprite = self.image_at(
                    (start_x, start_y, tile_size, tile_size), colorkey
                )

                images.append(sprite)

        return images
