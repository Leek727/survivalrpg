"""
NOT A SCRIPT - DO NOT RUN
Classes to help manage image data for all the different
objects and all the images associated with them.

*** IMPORTANT ***
A master item ID list is also present in this file. This is the location where all of the game's
tiles must be defined before they are able to be used. 
"""

if __name__ != "__main__":
    import pygame as pg
    from object_management import spritesheet

    # https://developer.mozilla.org/en-US/docs/Games/Techniques/Tilemaps

    class Object:
        def __init__(self, image_path, tile_size, ID, name, iscollision):
            self.tile_size = tile_size
            self.image_path = image_path
            self.image = pg.image.load(image_path)
            self.image_size = self.image.get_width()
            self.tile_size = tile_size
            self.ID = ID
            self.is_collideable = iscollision
            self.name = name

    class Tiled_Object(Object):
        def __init__(self, image_path, tile_size, ID, name, iscollision):
            super().__init__(image_path, tile_size, ID, name, iscollision)
            self.sheet = spritesheet.Spritesheet(self.image).load_images(
                self.tile_size
            )  # , colorkey=(152, 142, 133))

    class Normal_Object(Object):
        def __init__(self, image_path, tile_size, ID, name, iscollision):
            super().__init__(image_path, tile_size, ID, name, iscollision)
            self.sprite = self.image
            self.sheet = [self.sprite]

    # Create an empty array with 100 spaces
    WORLD_TILES = [None] * 100

    # Here, we define the actual items that will be used in the game (DO NOT USE INDEX 0)
    # By the way, the paths used are relative to the main file, not this file
    WORLD_TILES[1] = Tiled_Object(
        r"object_management/sprites/tileset.png", 30, 1, "TEST TILE 1", False
    )
    WORLD_TILES[2] = Tiled_Object(
        r"object_management/sprites/tileset2.png", 30, 2, "TEST TILE 2", True
    )
    WORLD_TILES[3] = Normal_Object(
        r"object_management/sprites/object_1.png", 30, 3, "TEST TILE 3", True
    )
else:

    print("This file is not a script. Running it will do nothing.")
