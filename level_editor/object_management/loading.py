"""
A series of functions to aid in loading a game. Not a script.
"""

import pygame as pg
import pickle
from math import *
import rendering
from object_management.export import get_colliders
from buttons import Button


def handle_dict(dictionary, x, y) -> list:
    """
    This function is used to load different screens from the world dictionary.
    """
    try:
        return dictionary[f"{x}x{y}"]
    except KeyError:
        max_ = len(dictionary) ** 0.5
        # User is trying to change to screen outside of bounds
        if x < 0:
            return dictionary[f"{0}x{y}"]
        if x > max_:
            return dictionary[f"{max_}x{y}"]
        if y < 0:
            return dictionary[f"{x}x{0}"]
        if y > max_:
            return dictionary[f"{x}x{max_}"]


def load_buttons(
    WORLD_TILES, TILE_SIZE, screen, side_pad=15, x_pad=100, y_pad=70
) -> list:
    """
    This function is used to make and load buttons.
    Currently, it is lacking the functionality of returning a button class. (doesn't exist yet)
    """
    # TODO add functionality to the buttons (make a class?)
    buttons = []
    button_layer = pg.Surface((300, 1000))
    current_page = 1
    x_loc, y_loc = screen.get_rect().topright
    x_loc -= 315
    y_loc += 25

    for i in range(5):
        i += 1
        # Get the image and put it on a surface
        try:
            object_ = WORLD_TILES[i]
            image = object_.sheet[0]
        except AttributeError:
            # A none-type object exists in place of a world object
            continue

        button = pg.Surface((TILE_SIZE, TILE_SIZE))
        button_rect = button.get_rect()
        button.blit(image, (0, 0))

        # Transform the button to make it bigger
        button = pg.transform.scale(button, (50, 50))

        z = i - 1
        # Calculate the new position
        button_y_loc = floor(z / 3) * y_pad + side_pad
        button_x_loc = (z % 3) * x_pad + side_pad

        # Place the button on the button layer
        button_rect.topleft = (button_x_loc, button_y_loc)
        button_layer.blit(button, (button_rect.topleft))

        # Generate button position relative to sceen as a whole
        real_button_rect = pg.Rect(button_x_loc + x_loc, button_y_loc + y_loc, 50, 50)

        buttons.append(Button(real_button_rect, object_))

    screen.blit(button_layer, (x_loc, y_loc))
    return buttons


def load_world(file_name, TILE_SIZE) -> dict:
    """
    This function is used to simplify loading the world from a file.
    If it fails to load from a file, it will generate a new world.
    """
    try:
        # open the world from a binary
        with open(file_name, "rb") as config_dictionary_file:
            WORLD = pickle.load(config_dictionary_file)

        # try to load colliders
        try:
            with open("export/colliders.pkl", "rb") as file:
                WORLD["colliders"] = pickle.load(file)
        except FileNotFoundError:
            WORLD["colliders"] = export.get_colliders(WORLD["chunks"])

    except FileNotFoundError:
        print("WARNING: no world file was found... creating world")
        # world file was not found, making a new one
        WORLD = dict()
        WORLD["chunks"] = {
            f"{x}x{y}": rendering.create_grid_2D_array((1600, 900), TILE_SIZE)
            for x in range(2)
            for y in range(2)
        }
    return WORLD


def export_project(WORLD, file_name) -> None:
    "This function can be used to save the curent project to a file"
    with open(file_name, "wb") as file:
        pickle.dump(WORLD, file, pickle.HIGHEST_PROTOCOL)
