"""
MAIN GAME FILE FOR LEVEL EDITOR - ONLY RUN THIS FILE
"""

import pygame as pg
import rendering
from object_management.objects import WORLD_TILES
from object_management.export import export_world
from object_management.loading import handle_dict
from object_management.loading import load_buttons
from object_management.loading import load_world
from object_management.loading import export_project
from time import sleep

global TILE_SIZE
TILE_SIZE = 30


def main():
    # Define some parameters
    screen_size = (1900, 1000)
    level_surface = pg.Surface((1600, 900))
    surface_rect = level_surface.get_rect()
    surface_rect.topleft = (0, 0)

    # Create the screen
    pg.init()
    screen = pg.display.set_mode(screen_size, pg.FULLSCREEN)
    screen.fill((0, 0, 0))

    # Create or load dict for the screen
    global WORLD
    WORLD = load_world("world.project", TILE_SIZE)

    # Set the current active screen chunk and display it
    x_chunk, y_chunk = 0, 0
    grid_array = WORLD["chunks"][f"{x_chunk}x{y_chunk}"]
    rendering.render_screen(level_surface, grid_array, WORLD_TILES)
    screen.blit(level_surface, (surface_rect.topleft))

    # Set a current object
    current_object = WORLD_TILES[1]

    # Create buttons on screen and retrieve list of button objects
    buttons = load_buttons(WORLD_TILES, TILE_SIZE, screen)

    # Determine the number of rows and columns for our world
    # TODO make the world a set size that gets resized with the window
    rows = level_surface.get_width() // TILE_SIZE
    columns = level_surface.get_height() // TILE_SIZE

    # Display current screen before mainloop
    pg.display.flip()

    running = True
    while running:

        for event in pg.event.get():
            # Listen for keys to move screen
            if event.type == pg.KEYDOWN:
                if event.key:
                    if event.key == pg.K_UP:
                        y_chunk += 1
                    if event.key == pg.K_DOWN:
                        y_chunk -= 1
                    if event.key == pg.K_LEFT:
                        x_chunk -= 1
                    if event.key == pg.K_RIGHT:
                        x_chunk += 1
                    if event.key == pg.K_ESCAPE:
                        running = False
                    if event.key == pg.K_x:
                        export_world(WORLD, WORLD_TILES, TILE_SIZE)
                        running = False
                    if event.key == pg.K_l:
                        # draw colliders
                        for collider in WORLD["colliders"][f"{x_chunk}x{y_chunk}"]:
                            pg.draw.rect(level_surface, (255, 0, 0), collider)
                        pg.display.flip()
                        break

                # Load and render new screen
                grid_array = handle_dict(WORLD["chunks"], x_chunk, y_chunk)
                rendering.render_screen(level_surface, grid_array, WORLD_TILES)
                screen.blit(level_surface, (surface_rect.topleft))
                pg.display.flip()

            # Check to see if buttons have been pressed
            if event.type == pg.MOUSEBUTTONDOWN:
                pos = event.pos

                for button in buttons:
                    if button.check_if_clicked(pos):
                        current_object = WORLD_TILES[button.ID]

            if event.type == pg.QUIT:
                running = False

        # Mouse click event
        mouse_clicks = pg.mouse.get_pressed()
        if mouse_clicks[0] or mouse_clicks[2]:  # left or right click
            # Find the mouse position (snapped to the grid)
            grid_pos = [
                (pos - (pos % TILE_SIZE)) // TILE_SIZE for pos in pg.mouse.get_pos()
            ]

            try:
                if mouse_clicks[2]:
                    # Right click clears the squares
                    grid_array[grid_pos[1]][grid_pos[0]] = 0
                else:
                    # Set the corresponding square to some value (based on current object)
                    grid_array[grid_pos[1]][grid_pos[0]] = current_object.ID
            except IndexError:
                "This exception falls when the user clicks outside of the alloted area, \
                    this is an expected error"
                pass

            # Update the screen
            rendering.render_screen(level_surface, grid_array, WORLD_TILES)
            screen.blit(level_surface, (surface_rect.topleft))
            pg.display.flip()


if __name__ == "__main__":
    main()

    # Save the project file before quitting
    export_project(WORLD, "world.project")

    pg.quit()
