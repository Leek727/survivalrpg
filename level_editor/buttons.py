import pygame as pg
from object_management.objects import Object


class Button:
    def __init__(self, rect, object_):
        self.rect = rect
        self.object = object_
        self.image = self.object.sheet
        self.ID = self.object.ID

    def check_if_clicked(self, point):
        return self.rect.collidepoint(point)
