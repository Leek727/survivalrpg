"""
Functions to aid in rendering for the level editor
"""
import pygame as pg


def create_grid_2D_array(screen_size, square_size):
    """
    The following code generates an array based on screen_size and square_size,
    such that a grid of squares with side length square_size can fit.
    All values in this array are intialized to 0
    """

    width, height = screen_size
    # Determine array size
    rows = width // square_size
    columns = height // square_size
    # Create array
    result = []
    for _i in range(columns):
        result.append([0] * rows)

    return result


def get_boundedness(array, x, y):
    """
    The following code works by checking each side of the given square (at x, y)
    each side is represented by a digit in binary
    at the end of the function, this binary value is converted to an int of base 10

    A really good explanation of this concept is linked below:
    https://web.archive.org/web/20150906102436/http://www.saltgames.com/2010/a-bitwise-method-for-applying-tilemaps/
    """

    directions = {"above": (0, -1), "right": (1, 0), "below": (0, 1), "left": (-1, 0)}

    boundedness = ""
    for direction in directions.values():
        try:
            # create positions that we need to check
            x_change, y_change = direction
            checked_x, checked_y = x + x_change, y + y_change

            # If something exists at this spot, it is bounded
            if array[checked_y][checked_x] == array[y][x]:
                boundedness += "1"
            else:
                boundedness += "0"

        # if the spot checked is outside of bounds count it as bounded
        except IndexError:
            boundedness += "1"

    # return binary boundedness in base 10
    return int(boundedness[::-1], 2)


def render_screen(screen, array2D, WORLD_TILES):
    """
    The following code renders each grid on the screen
    currently this is only being applied to one square
    a more thorough system will be needed with the implementation
    of more types of squares
    """
    screen.fill((0, 255, 0))
    # Double enumerate to access each grid item on screen
    for y, row in enumerate(array2D):
        for x, square in enumerate(row):

            # Check that the square is not empty (0 casts to False, all other ints cast to True)
            if square:
                tile_type = WORLD_TILES[square]

                # Get the sprite for the tile
                if tile_type.__class__.__name__ == "Tiled_Object":
                    boundedness = get_boundedness(array2D, x, y)
                    sprite = tile_type.sheet[boundedness]
                else:
                    sprite = tile_type.sprite

                # Blit the square to screen
                screen.blit(sprite, (x * 30, y * 30))


# UNUSED, NOT WORKING
def render_grid(tile_size, screen, grid_size):
    width, height = screen.get_width(), screen.get_height()
    grid_x, grid_y = grid_size

    for x in range(width - grid_x, width, tile_size):
        for y in range(height - grid_y, height, tile_size):
            rect = pg.Rect(x, y, tile_size, tile_size)
            pg.draw.rect(screen, (255, 255, 255, 105), rect, 1)
