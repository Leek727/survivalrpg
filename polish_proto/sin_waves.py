import random
import numpy as np
import pygame as pg
from pygame.time import Clock


class Particle:
    def __init__(
        self, screen, color=(255, 0, 0), speed=8, size=(8, 8), bounds=(20, 410), dire=1
    ) -> None:
        if dire == 2:
            self.DIR = -1
        else:
            self.DIR = 1
        self.SIZE = size
        self.BOUNDS = bounds
        self.SPEED = speed
        self.COLOR = color
        self.screen = screen
        self.pos = pg.Rect(random.randint(bounds[0], bounds[0] + 14), 0, *size)
        self.height = random.randint(700, 720)

    def summon(self, travel=True):
        if travel:
            self.travel()
        pg.draw.rect(self.screen, self.COLOR, self.pos, border_radius=8)

    def travel(self):
        self.pos.centery = (
            6
            * (
                self.DIR
                * np.sin(self.pos.centerx / 60)
                * np.sin(np.sqrt(self.pos.centerx))
            )
            + self.height
        )
        self.pos.centerx += self.SPEED


screen = pg.display.set_mode((1200, 800))
CLOCK = pg.time.Clock()

particles = [
    Particle(screen, speed=random.uniform(5.5, 9), dire=random.randint(1, 2))
    for _ in range(14)
]

run = True
while run:
    screen.fill((0, 0, 0))

    keys = pg.key.get_pressed()
    for event in pg.event.get():
        if event.type == pg.QUIT:
            run = False
        elif event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                run = False

            if event.key == pg.K_q:
                particles = [
                    Particle(
                        screen, speed=random.uniform(5.5, 8), dire=random.randint(1, 2)
                    )
                    for _ in range(10)
                ]

    pg.draw.rect(screen, (0, 255, 0), pg.Rect(20, 695, 400, 30), border_radius=5)

    for particle in particles:
        particle.summon()
        if (
            particle.pos.centerx > particle.BOUNDS[1]
            or particle.pos.centerx < particle.BOUNDS[0]
        ):
            particles.remove(particle)

    pg.display.update()
    CLOCK.tick(45)


pg.quit()
