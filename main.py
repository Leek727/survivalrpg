from math import pi
import pickle
import pygame as pg
from random import randint
from front_assets import Character
from front_assets.World import World
from front_assets.engine import make_item
from front_assets.status import render_status
from front_assets.particles import SBParticle
from front_assets.Menus import DeathMenu, EscMenu, StartMenu

# CONSTANTS
ASSET_PATH = "front_assets/assets"
WORLD_PATH = f"{ASSET_PATH}/worlds/export"
WIN_SIZEX = 1590
WIN_SIZEY = 900
clock = pg.time.Clock()


def main() -> None:
    # Create Window
    pg.init()
    screen = pg.display.set_mode((WIN_SIZEX, WIN_SIZEY))
    background_screen = pg.Surface((WIN_SIZEX, WIN_SIZEY))
    pg.display.flip()

    # Start menu. Waits unitll player hits play
    StartMenu(("Play", "Quit"), screen, clock).summon()
    player_data = pickle.load(open(f"{WORLD_PATH}/character.pkl", "rb"))
    # Create character
    player = Character.Character(
        screen,
        pg.image.load(f"{ASSET_PATH}/player.png").convert(),
        player_data["pos"],
        player_data["chk"],
        player_data["stats"],
    )

    # Init the world
    world = World(background_screen, player)
    player.world = world

    player.update_taskbar()
    player.summon()
    # Main game loop
    running = True
    while running:
        screen.blit(background_screen, (0, 0))
        # Check PyGame events
        # Cant hold keys
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False

            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    escmenu = EscMenu(("Save", "Return", "Quit"), screen, world, clock)
                    escmenu.summon()
                    if escmenu.exit:
                        running = False
                      
                
                # Place item when player presses 'e'
                if event.key == pg.K_q:
                    player.place_item(world.items)

                if event.key == pg.K_f:
                    player.use_item()

        # Render health and oxygen
        render_status(
            screen,
            status_scale=1,
            screenx=WIN_SIZEX,
            screeny=WIN_SIZEY,
            character=player,
        )
        # Do input checking for keys
        keys_pressed = pg.key.get_pressed()
        # Render player
        player.handle_keys(keys_pressed, world)
        # Handle world logic
        world.main(keys_pressed)

        # every couple hundred frames, make some cool particles
        if randint(1, 250) == 50:
            for _ in range(randint(8, 20)):
                world.particles.append(SBParticle(screen, (101, WIN_SIZEY - 40)))

        pg.display.update()
        clock.tick(60)


# Begin main function
if __name__ == "__main__":
    main()
    pg.quit()
