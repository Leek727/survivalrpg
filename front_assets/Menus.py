import sys
import pygame as pg

from front_assets.World import World

"""This file is only for custom menu classes"""

# Custom Exception to break out of menus. The only time this should be used is to return to main while loop
class Return(Exception):
    pass


# Parent menu class
class Menu:
    def __init__(
        self, options, screen, clock, color, select_color, font_color, font
    ) -> None:
        # CONSTANTS
        # The screen to blit the menu to
        self.SCREEN = screen
        self.CLOCK = clock
        self.FONT = font
        self.FONT_COLOR = font_color
        self.COLOR = color
        self.SELECT_COLOR = select_color
        self.options = options
        self.selected = 1
        self.exit = False # Var for determining if the player exited the menu or exited the game

    def summon(self) -> None:
        """Creates menu while loop"""
        running = True
        try:
            while running:
                self.SCREEN.fill((231, 125, 17))

                # Blit all the options onto the screen
                for i, val in enumerate(self.renders):
                    render, pos = val
                    border = pg.Rect(50, 100, 200, 70)
                    border.topleft = (pos[0] - 10, pos[1] - 10)
                    if i == self.selected:
                        color = self.SELECT_COLOR

                    else:
                        color = self.COLOR

                    pg.draw.rect(self.SCREEN, color, border)
                    self.SCREEN.blit(render, pos)
                # EVENT HANDLING
                for event in pg.event.get():
                    if event.type == pg.QUIT:
                        pg.quit()
                        exit()

                    if event.type == pg.KEYDOWN:
                        if event.key == pg.K_ESCAPE:
                            running = False

                        if event.key == pg.K_DOWN:
                            self.next_option()

                        if event.key == pg.K_UP:
                            self.prev_option()

                        if event.key == pg.K_RETURN:
                            self.handle_functions()

                pg.display.update()
                self.CLOCK.tick(60)
        except:
            pass

    def next_option(self) -> None:
        """Changes menu selection to next button"""
        if self.selected + 1 < len(self.options):
            self.selected += 1

    def prev_option(self) -> None:
        """Change menu selection to previous button"""
        if self.selected - 1 >= 0:
            self.selected -= 1

    def get_current_op(self) -> str:
        """Return whatever is currently selected"""
        return self.options[self.selected]

    def create_renders(self) -> None:
        """Goes through each text in options and creates renders for them"""
        self.renders = []
        posY = int(self.SCREEN.get_height() / 2 - 200)
        posX = int(self.SCREEN.get_width() / 2 - 100)
        for i, text in enumerate(self.options):
            self.renders.append(
                (self.FONT.render(text, 1, self.FONT_COLOR), (posX, posY + i * 100))
            )


class EscMenu(
    Menu,
):
    def __init__(self, options, screen, world, clock) -> None:
        # Set up the menu
        super().__init__(
            options,
            screen,
            clock,
            (0, 0, 0),  # Color of border
            (102, 102, 153),  # Color of border when selected
            (255, 255, 255),  # Color of font
            pg.font.Font(f"front_assets/assets/SUPERSCR.TTF", 50),
        )
        self.world = world
        # Colors of the rects around the options
        self.create_renders()

    def handle_functions(self) -> None:
        """Handles whatever the user selects"""
        if self.get_current_op() == "Return":
            raise Return
        if self.get_current_op() == "Save":
            self.world.save_chunk()
            self.world.save_char()
        if self.get_current_op() == "Quit":
            self.world.save_chunk()
            self.world.save_char()
            self.exit = True
            raise Return


class StartMenu(Menu):
    def __init__(self, options, screen, clock) -> None:
        super().__init__(
            options,
            screen,
            clock,
            (0, 0, 0),  # color of border
            (102, 102, 153),  # color of border when selected
            (255, 255, 255),  # color of font
            pg.font.Font(f"front_assets/assets/SUPERSCR.TTF", 50),
        )
        self.create_renders()

    def handle_functions(self) -> None:
        """Handles whatever the user selects"""
        if self.get_current_op() == "Play":
            raise Return
        if self.get_current_op() == "Exit":
            sys.exit()


class DeathMenu():
    def __init__(self, screen, clock) -> None:
        self.SCREEN = screen
        self.IMAGE = pg.image.load('front_assets/assets/DeathMenu.png').convert()
        self.OPTIONS = ['Respawn', 'Exit Game (looser)']
        self.FONT = pg.font.Font(f"front_assets/assets/SUPERSCR.TTF", 50)
        self.FONT_COLOR = (128, 128, 128)
        self.COLOR = (0, 0, 0)
        self.SELECT_COLOR = (102, 102, 153)
        self.CLOCK = clock;
        self.selected = 1
        self.death_msg = 'YOU DIED'
        self.create_renders()

    def summon(self) -> None:
        """Creates menu while loop"""
        running = True
        try:
            while running:

                #self.SCREEN.fill((0, 0, 0))
                self.SCREEN.blit(self.IMAGE, (0, 0))
                # blit the death msg
                self.SCREEN.blit(self.death_msg[0], self.death_msg[1])

                # Blit all the options onto the screen
                for i, val in enumerate(self.renders):
                    render, pos = val
                    border = pg.Rect(50, 100, 200, 70)
                    border.topleft = (pos[0] - 10, pos[1] - 10)
                    if i == self.selected:
                        color = self.SELECT_COLOR

                    else:
                        color = self.COLOR

                    pg.draw.rect(self.SCREEN, color, border)
                    self.SCREEN.blit(render, pos)
                # EVENT HANDLING
                for event in pg.event.get():
                    if event.type == pg.QUIT:
                        pg.quit()
                        exit()

                    if event.type == pg.KEYDOWN:
                        if event.key == pg.K_ESCAPE:
                            running = False

                        if event.key == pg.K_DOWN:
                            self.next_option()

                        if event.key == pg.K_UP:
                            self.prev_option()

                        if event.key == pg.K_RETURN:
                            self.handle_functions()

                pg.display.update()
                self.CLOCK.tick(60)
        except ImportError:
            pass


    def handle_functions(self) -> None:
        """Handles whatever the user selects"""
        if self.get_current_op() == "Exit Game (looser)":
            sys.exit()



    def next_option(self) -> None:
        """Changes menu selection to next button"""
        if self.selected + 1 < len(self.OPTIONS):
            self.selected += 1

    def prev_option(self) -> None:
        """Change menu selection to previous button"""
        if self.selected - 1 >= 0:
            self.selected -= 1

    def get_current_op(self) -> str:
        """Return whatever is currently selected"""
        return self.OPTIONS[self.selected]

    def create_renders(self) -> None:
        """Goes through each text in options and creates renders for them"""
        self.renders = []
        posY = int(self.SCREEN.get_height() / 2 - 200)
        posX = int(self.SCREEN.get_width() / 2 - 100)
        for i, text in enumerate(self.OPTIONS):
            self.renders.append(
                (self.FONT.render(text, 1, self.FONT_COLOR), (posX, posY + i * 100))
            )
        self.death_msg = (self.FONT.render(self.death_msg, 1, (255, 255, 255)), (posX, 100))




