import enum
import math
import time
import numpy as np
import pygame as pg
from pygame.sprite import RenderUpdates


def iload(path):
    """
    Takes a path and returns a loaded pygame image.
    Just a shortcut for pg.image.load().convert()
    """
    return pg.image.load(path).convert()

class EnemyTypes(str, enum.Enum):
    ENEMYV1 = "enemyv1"


class Enemy:
    def __init__(self, screen, thresh) -> None:
        # MAx distance that enemy will go to the player
        self.THRESH = thresh
        self.screen = screen
        self.start = time.time()

    def summon(self) -> None:
        self.image = self.find_direction()
        self.last_pos = self.pos.center
        self.screen.blit(self.image, self.pos.topleft)

    def _set_pos(self, pos):
        """DO NOT CALL THIS FUNCTION. ONLY TO BE USED IN THE INIT OF AN ENEMY CLASS"""
        for d in self.images:
            self.images[d].set_colorkey((255, 255, 255))
        self.pos = self.images['f'].get_rect()
        self.pos.bottomright = pos
        self.image = self.images['f']
        self.last_pos = pos
    
    def find_direction(self) -> str:
        """Find which direction the enemy is moving"""

        if self.pos.center == self.last_pos: return self.images['f'] # hasn't moved

        # movement on y and x
        dy, dx = self.pos.centery - self.last_pos[1], self.pos.centerx - self.last_pos[0]
        if dx != 0:
            slope = dy / dx         
        else: # slope is undefined e.i. vertical line
            if dy < 0:
                return self.images['b']
            return self.images['f']

        if slope >= -1 and slope <= 1: 
            if dx < 0: # player is moving left because x is decreasing
                return self.images['l']
            return self.images['r']
        if dy < 0:
            return self.images['b']
        return self.images['f']
            

        # if self.pos.center == self.last_pos:
        #     return self.images['f']
    
        # if self.pos.centerx - self.last_pos[0] < 0:
        #     return self.images['l']
        
        # elif self.pos.centerx - self.last_pos[0] > 0:
        #     return self.images['r']

        # return self.images['f']


    def attack(self, player) -> None:
        """
        Player health - enemy damage. Will only attack
        once every interval specified in self.COOLDOWN
        """

        if time.time() - self.start >= self.COOLDOWN:
            player.stats["hp"] -= self.DAMAGE
            self.start = time.time()

    def move_towards_player(self, player, walls):
        """Use this method for enemy movement"""
        # find x and y distance between enemy and player
        dx, dy = (
            player.position.x - self.pos.x,
            player.position.y - self.pos.y,
        )
        # Find the exact distance between the points
        # Find the amount of distance to cover each frame
        dist = math.hypot(dx, dy)
        if dist > 15:
            if self._check_for_walls(walls, player):
                dx, dy = dx / dist, dy / dist
                self.pos.x += dx * self.SPEED
                self.pos.y += dy * self.SPEED

    def _check_for_walls(self, walls, player) -> bool:
        points = self._get_line(*player.position.center, *self.pos.center)[::15]
        for point in points:
            for wall in walls:
                if wall.collidepoint(point):
                    return False
        return True

    def _get_line(self, x1, y1, x2, y2):
        points = []
        issteep = abs(y2 - y1) > abs(x2 - x1)
        if issteep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2
        rev = False
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1
            rev = True
        deltax = x2 - x1
        deltay = abs(y2 - y1)
        error = int(deltax / 2)
        y = y1
        ystep = None
        if y1 < y2:
            ystep = 1
        else:
            ystep = -1
        for x in range(x1, x2 + 1):
            if issteep:
                points.append((y, x))
            else:
                points.append((x, y))
            error -= deltay
            if error < 0:
                y += ystep
                error += deltax
        # Reverse the list if the coordinates were reversed
        if rev:
            points.reverse()
        return points


class Enemyv1(Enemy):
    def __init__(self, screen, pos) -> None:
        super().__init__(screen, thresh=130)
        self.COOLDOWN = 1
        self.NAME = "Enemyv1"
        self.SPEED = 6
        self.DAMAGE = 2
        self.IPATH = 'front_assets/assets/enemy_sprites/'
        self.INAME = 'enemyV1' # name of the image excluding the direction
        print(iload(f'{self.IPATH}{self.INAME}f.png'))
        self.images = {
            'f' : pg.transform.scale2x(iload(f'{self.IPATH}{self.INAME}f.png')),
            'b' : pg.transform.scale2x(iload(f'{self.IPATH}{self.INAME}b.png')),
            'l' : pg.transform.scale2x(iload(f'{self.IPATH}{self.INAME}l.png')),
            'r' : pg.transform.scale2x(iload(f'{self.IPATH}{self.INAME}r.png')),
        }
        self._set_pos(pos)

    @staticmethod
    def name() -> str:
        """Return enemy name as a string instead of enum"""
        return "Enemyv1"
