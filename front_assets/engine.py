import random
import pygame as pg
from front_assets import Item
from typing import Dict, Tuple
from front_assets.Enemies import EnemyTypes, Enemyv1

"""THIS FILE IS FOR HELPER FUNCTIONS THAT MAY BE NEEDED IN MULTIPLE FILES"""


def make_item(items, items_to_add, screen) -> Dict:
    """
    This function will take a item type, an amount and a location, then create that amount of that type of item. e.g. -
    make_item(items, {"ItemType1" : (1, (100, 100))}, screen)
    This will make 1 ItemType1 item at postion 100 100. You can add as many keys as you like
    """
    item_types = {
        "ItemType1": Item.ItemType1,
        "ItemType2": Item.ItemType2,
        "Medkit": Item.Medkit,
    }

    for item in items_to_add.keys():
        # Check if item exists, then create the items and append them to the
        # actual items dict
        if item in item_types.keys():
            amount, pos = items_to_add[item]
            # If there are no items of that type in the items dict, add an empty list theres
            if item not in items.keys():
                items[item] = []

            for _ in range(amount):
                items[item].append(item_types[item](screen, pos))

        else:
            # whoops invalid item dumbo
            raise Exception(
                f"{item} Type Item Does Not Exist. Current items are {item_types.keys()}"
            )
    return items


def make_enemy(enemies, create, screen) -> Dict:
    """
    Takes current enemy dict, enemies to add, and screen e.g
    make_enemy(enemies, (EnemyTypes.ENEMYV1, (100, 100)), screen) -> Dict with new enemies
    """
    enemy_types = {EnemyTypes.ENEMYV1: Enemyv1}

    enemy, pos = create
    if enemy not in enemy_types:
        raise Exception(
            f'Invalid enemy name "{enemy}"\nCurrent enemy types can be seen in the EnemyTypes class in file Enemies.py'
        )

    if enemy not in enemies:
        enemies[enemy] = []
    # add the enemy to the main dict
    enemies[enemy].append(enemy_types[enemy](screen, pos))
    return enemies


def create_grid_2D_array(
    screen_size: Tuple[int, int], square_size: int, random=False
) -> Dict:
    """
    The following code generates an array based on screen_size and square_size,
    such that a grid of squares with side length square_size can fit.
    All values in this array are intialized to 0 if random is set to false
    If random is true, then it will either be 1 or 0
    """

    width, height = screen_size
    # Determine array size
    rows = width // square_size
    columns = height // square_size
    # Create array
    result = []
    for _ in range(columns):
        if random:
            result.append([random.randint(0, 1000) % 2] for i in range(rows))
        else:
            result.append([0] * rows)
    return result
