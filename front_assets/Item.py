import pygame as pg
import numpy as np

from front_assets import status

## THIS FILE STILL NEEDS SPRITES... OBVIOUSLY
# Parent Class. All items should inherit from this one
class Item:
    def __init__(self, screen, position, size) -> None:
        # CONSTANTS

        # Create Rect object that represents the item and the hitbox
        self.position = pg.Rect(0, 0, *size)
        self.screen = screen
        # Give initial position
        self.position.center = position

    def summon(self) -> None:
        pg.draw.rect(self.screen, self.COLOR, self.position)

    def write_text(self):
        pos = list(self.position.midtop)
        pos[0] -= 20
        pos[1] -= 35
        
        self.screen.blit(self.text, pos)


#### THIS IS WHERE ALL THE ITEM CLASSES WILL GO. ####
#### LIKELY I WILL MAKE IT SO THAT ATRIBUTES CAN BE LOADED FROM A JSON FILE
#### BUT FUNCTIONS WILL HAVE TO BE HARD CODED IN UNFORTUNATLY


class ItemType1(Item):
    def __init__(self, screen, position) -> None:
        # CONSTANTS
        self.SIZE = (50, 50)
        self.NAME = "ItemType1"

        # Inherit all of the Item class traits
        super().__init__(screen, position, self.SIZE)
        self.COLOR = (100, 255, 100)
        # Text to write on the item. This is just for debug reasons.
        self.font = pg.font.SysFont("Arial", 15)
        self.text = self.font.render("Item 1", True, (240, 231, 231))

    # Put any special charateristics of this item here
    def summon(self):
        super().summon()
        self.write_text()


class ItemType2(Item):
    def __init__(self, screen, position) -> None:
        self.SIZE = (30, 60)
        self.NAME = "ItemType2"

        # Inherit all of the Item class traits
        super().__init__(screen, position, self.SIZE)
        self.COLOR = (20, 25, 200)
        # Text to write on the item. This is just for debug reasons.
        self.font = pg.font.SysFont("Arial", 15)
        self.text = self.font.render("Item 2", True, (240, 231, 231))

    # Put any special charateristics of this item here
    def summon(self):
        super().summon()
        self.write_text()


class Medkit(Item):
    def __init__(self, screen, position) -> None:
        self.SIZE = (60, 30)
        self.NAME = "Medkit"

        # Inherit all of the Item class traits
        super().__init__(screen, position, self.SIZE)
        self.IMAGE = pg.transform.scale2x(pg.image.load('front_assets/assets/medNeedle.png')).convert()
        self.IMAGE.set_colorkey((255, 255, 255))

        # Text to write on the item. This is just for debug reasons.
        self.font = pg.font.SysFont("Arial", 15)
        self.text = self.font.render("Medkit", True, (240, 231, 231))

    # Put any special charateristics of this item here
    def summon(self):
        """Blit to screen"""
        #super().summon()
        self.screen.blit(self.IMAGE, self.position.topleft)
        self.write_text()

    @staticmethod
    def special(character):
        """The Special for medkit adds 20 health to the character"""
        MEDKIT_VAL = 20  # how much hp to give
        if character.stats["hp"] < 100:
            if character.stats["inv"]["Medkit"] >= 1:
                # add ass much health as possible - if hp = 120 clip to 100
                character.stats["hp"] = np.clip(
                    character.stats["hp"] + MEDKIT_VAL, 0, character.max_stats["hp"]
                )
                character.stats["inv"]["Medkit"] -= 1
                # If the medkit amount is 0, remove from inventory
                if character.stats["inv"]["Medkit"] < 1:
                    character.stats["inv"].pop("Medkit")
