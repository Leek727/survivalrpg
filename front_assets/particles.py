import random
import numpy as np
import pygame as pg


class Particle:
    def __init__(self, screen, color, pos, size, speed=None) -> None:
        self.COLOR = color
        self.SPEED = speed
        self.screen = screen
        self.pos = pg.Rect(*pos, *size)

    def summon(self):
        self.travel()
        pg.draw.rect(self.screen, self.COLOR, self.pos, border_radius=7)


class PhysParticle(Particle):
    """A particle that has collision or interacts with the world"""

    def __init__(self) -> None:
        super().__init__()


class SBParticle(Particle):
    """Particle for the status bar"""

    def __init__(self, screen, pos) -> None:
        super().__init__(
            screen, (128, 255, 128), pos, (11, 11), speed=random.uniform(5.5, 9)
        )
        self.DIR = (
            1 if random.randint(0, 100) <= 50 else -1
        )  # Randomly select if the sine wave is normal or inverse
        self.boundsx = (100, screen.get_width() / 3 + random.randint(25, 75))
        # there is essentially no y axis bounds for this particle, so just set it to top/bottom of screen
        self.boundsy = (0, screen.get_height())
        # set the height of the particle, because otherwise it would be overwritten in the sine function
        self.height = pos[1]

    def travel(self):
        """The algorithm that defines where the particle should travel"""
        self.pos.centery = (
            6
            * (
                self.DIR
                * np.sin(self.pos.centerx / 60)
                * np.sin(np.sqrt(self.pos.centerx))
            )
            + self.height
        )
        self.pos.centerx += self.SPEED
