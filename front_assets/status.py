import pygame as pg

# Create renders for health and oxygen
pg.init()
myfont = pg.font.Font(f"front_assets/assets/SUPERSCR.TTF", 20)
HEALTH_RENDER = myfont.render("Health ", False, (0, 0, 0))  # text
OXY_RENDER = myfont.render("Oxygen ", False, (0, 0, 0))


def render_status(screen, status_scale, screenx, screeny, character):
    """Render health and oxygen status"""
    # get character stats
    health = character.stats["hp"]
    oxygen = character.stats["oxygen"]

    # mult health percentage with total possible health
    pg.draw.rect(
        screen,
        (255, 0, 0),
        (
            100,
            screeny - 50,
            status_scale * (screenx / 3) * (health / 100),
            status_scale * (25),
        ),
        border_radius=10,
    )  # bar
    screen.blit(HEALTH_RENDER, (20, screeny - 50))

    # oxygen
    pg.draw.rect(
        screen,
        (0, 0, 255),
        (
            100,
            screeny - 75,
            status_scale * (screenx / 3) * (oxygen / 100),
            status_scale * (25),
        ),
        border_radius=10,
    )
    screen.blit(OXY_RENDER, (20, screeny - 75))
