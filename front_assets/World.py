from enum import EnumMeta
import pickle
import numpy as np
import pygame as pg
from random import randint
from front_assets.Enemies import Enemyv1, EnemyTypes
from front_assets.engine import make_item, make_enemy

"""
This File is for the World.
"""

class World:
    def __init__(self, screen, player) -> None:
        # Path to world
        self.PATH = "front_assets/assets/worlds/export"
        self.BACKGROUND_PATH = f"{self.PATH}/backgrounds"
        self.screen = screen  # THIS IS BACKGROUND PLS DONT FORGET. USE self.player.screen for main screen
        self.player = player
        self.items = {}  # These are the items that are in the current chunk
        self.enemies = {}  # enemies in current chunk
        self.particles = []

        self.update_background()
        self.update_items()

    def main(self, keys_pressed):
        """This is the main function for the world class that should be called from main.py"""
        self.keys_pressed = keys_pressed
        self.check_collision()
        self.handle_particles()
        self.handle_items()
        self.handle_enemies()

    # -------------- QUALITY OF LIFE FUNCTIONS ---------------------- #
    def get_walls(self) -> list:
        """Returns colliders for the inputed chunk."""
        with open(f"{self.PATH}/colliders.pkl", "rb") as f:
            self.all_colliders = pickle.load(f)
        return self.all_colliders[self.cur_chunk()]

    def cur_chunk(self) -> str:
        return f"{self.player.chunk[0]}x{self.player.chunk[1]}"

    # ------------ UPDATE FUNCTIONS ------------------ #
    def update_items(self):
        """Updates self.items to the current chunks items"""
        with open(f"{self.PATH}/items.pkl", "rb") as f:
            self.all_items = pickle.load(f)  # Items for all the chunks
        self.items = {}
        # Create the items from just strings to enemy objects so that the program can actually use them
        for itemtype in self.all_items[self.cur_chunk()]:
            for pos in self.all_items[self.cur_chunk()][itemtype]:
                self.items = make_item(
                    self.items, {itemtype: (1, pos)}, self.player.screen
                )

    def update_background(self):
        """Blits the chunk background image onto the screen provided, and loads chunk items"""
        self.screen.fill((0, 0, 0))
        # Load the appropriate background screen
        image = pg.image.load(
            f"{self.BACKGROUND_PATH}/{self.cur_chunk()}_background.png"
        ).convert()
        self.screen.blit(image, (0, 0))
        self.update_items()
        self.update_enemies()

    def update_enemies(self):
        """Update enemies to the current chunk"""
        with open(f"{self.PATH}/enemies.pkl", "rb") as f:
            self.all_enemies = pickle.load(f)

        self.enemies = {}
        # Turn the enemy positions and types into enemy classes
        for enemy_type in self.all_enemies[self.cur_chunk()]:
            for pos in self.all_enemies[self.cur_chunk()][enemy_type]:
                self.enemies = make_enemy(
                    self.enemies,
                    (EnemyTypes[enemy_type.upper()], pos),
                    self.player.screen,
                )

    # --------------- FUNCTIONS FOR UPDATING WORLD THINGS ------------------- #
    def handle_enemies(self) -> None:
        """Moves all the enemies and handles attacks"""
        p_coords = self.player.position.center
        for enemytype in self.enemies:
            for enemy in self.enemies[enemytype]:

                # check distance between player and enemy
                if (
                    np.sqrt(
                        (enemy.pos.centerx - p_coords[0]) ** 2
                        + (enemy.pos.centery - p_coords[1]) ** 2
                    )
                    < enemy.THRESH
                ):
                    enemy.attack(self.player)

                enemy.move_towards_player(self.player, self.get_walls())
                enemy.summon()

    def handle_items(self) -> None:
        # Go through each type of item in the items list and summon it or pick it up
        for item_type in self.items.keys():
            for item in self.items[item_type]:
                if (
                    item.position.colliderect(self.player.position)
                    and self.keys_pressed[ord("e")]
                ):
                    self.items[item_type].remove(item)
                    self.player.pick_item(item)
                    self.save_char()
                item.summon()

    def handle_particles(self) -> None:
        """Basicly just calls .summon() on each particle and checks bounds"""
        for particle in self.particles:
            particle.summon()

            if (
                not particle.boundsx[0] < particle.pos.centerx < particle.boundsx[1]
            ) or (not particle.boundsy[0] < particle.pos.centery < particle.boundsy[1]):
                # particle x or y is not between the bounds, delete it
                self.particles.remove(particle)

    def check_collision(self):
        """Checks collision between player and walls"""
        for rect in self.get_walls():
            if self.player.position.colliderect(rect):
                pg.draw.rect(
                    self.player.screen, (255, 0, 0), pg.Rect(*rect.center, 10, 10)
                )

    # ------------------- SAVING FUNCTIONS --------------------- #
    def save_char(self) -> None:
        """Save the player"""
        save_dict = {
            "stats": self.player.stats,
            "pos": self.player.position.bottomright,
            "chk": self.player.chunk,
        }
        with open(f"{self.PATH}/character.pkl", "wb") as f:
            pickle.dump(save_dict, f)

    def save_chunk(self):
        """Save the chunk to all the pkl files"""
        item_save_dict = {}
        # This goes through each item type, then saves the position of each item to a list
        # That list is then stored in the appropriate chunk in the self.all_items dict
        # self.items : item classes -> item_save_dict[itemtype] : list [tuple[int, int]] do that for each itemtype
        #  -> all_items[cur_chunk] -> pkl dump
        with open(f"{self.PATH}/items.pkl", "wb") as item_file:
            for itemtype in self.items:  # enumerate all the items stored rn
                item_save_dict[itemtype] = []
                for item in self.items[itemtype]:
                    item_save_dict[itemtype].append(item.position.center)

            # update all items dict with the newly saved items
            self.all_items[self.cur_chunk()] = item_save_dict
            pickle.dump(self.all_items, item_file)

        # update all the enemies to make sure it is correct and up to date
        with open(f"{self.PATH}/enemies.pkl", "rb") as f:
            self.all_enemies = pickle.load(f)
        # Turns enemies into saveble forms then save. Exact same as the items
        # self.enemies : enemy classes -> self.all_enemies[current chunk][enemy type] : list [tuple[int, int] these are the positions] -> pickle dump
        with open(f"{self.PATH}/enemies.pkl", "wb") as f:
            self.all_enemies[self.cur_chunk()] = {
                EnemyTypes[enemy_type.upper()].value: [
                    enemy.pos.center for enemy in self.enemies[enemy_type]
                ]
                for enemy_type in self.enemies
            }
            pickle.dump(self.all_enemies, f)
