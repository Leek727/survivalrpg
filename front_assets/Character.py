from os import device_encoding
import pygame as pg
from typing import Dict, List
from front_assets.Item import Medkit
from front_assets.engine import make_item
import math
import numpy as np

class Character(pg.sprite.Sprite):
    def __init__(self, screen, image, position, chunk: "list[int, int]", stats) -> None:
        # CONSTANTS
        self.SPEED = 4.5
        # The first font is for the taskbar, and the 2nd one is for the taskbar msg
        self.FONTS = [pg.font.SysFont("Arial", 60), pg.font.SysFont("Arial", 25)]

        # Inherit from sprite class
        pg.sprite.Sprite.__init__(self)

        # This function makes all white pixels in the image transparent.
        image.set_colorkey((255, 255, 255))
        self.image = image

        # init given values
        self.screen = screen
        self.position = self.image.get_rect()
        self.position.bottomright = position
        self.world = None
        # The chunk that the player is currently in
        self.chunk = chunk
        # Load a sprite object
        self.sprite = pg.sprite.Sprite()
        self.sprite.image = self.image
        # Create momentum variables
        self.x_momentum = 0
        self.y_momentum = 0

        # Player stats are in an object so they can be easily saved.
        self.stats = stats
        self.max_stats = {"oxygen": 100, "hp": 100}

        # The currently selected inventory slot
        self.slot_selected = 1
        # Create taskbar render
        self.taskbar = [
            self.FONTS[0].render(str(i), True, (0, 0, 0)) for i in range(1, 4)
        ]
        # This is the red thing that comes above the taskbar
        self.taskbar_msg = self.FONTS[1].render(f"", True, (0, 0, 255))

        # dictionary for collision
        self.collidict = {'left': 0, 'bottom': 0, 'right': 0, 'top': 0}
        self.DEBUG = False

    def summon(self) -> None:
        # Blit self onto screen
        self.screen.blit(self.sprite.image, self.position.topleft)

        # Blit taskbar
        self.screen.blit(
            self.taskbar_msg,
            ((self.screen.get_width() / 2), (self.screen.get_height() - 100)),
            ################################      CHANGE THIS NUMBER HERE ^ TO CHANGE THE HEIGHT OF TASKBAR MESSAGE
        )
        for i, slot in enumerate(self.taskbar):
            self.screen.blit(
                slot,
                (
                    self.screen.get_width() / 2 + (i * 70),
                    self.screen.get_height() - 75,
                ),
                ######  CHANGE THIS NUMBER HERE ^ TO CHANGE THE HEIGHT OF TASKBAR
            )

    # ----------- POSITION FUNCTIONS -----------#
    def check_collision(self, rect1, rect2) -> dict:
        """Checks direction of collision between two rectangles"""
        temp_collisions = {'left': 0, 'bottom': 0, 'right': 0, 'top': 0}
        if rect1.midtop[1] > rect2.midtop[1]:
            temp_collisions["top"] += 1

        elif rect1.midleft[0] > rect2.midleft[0]:
            temp_collisions["left"] += 1
                    
        elif rect1.midright[0] < rect2.midright[0]:
            temp_collisions["right"] += 1
                    
        else:
            temp_collisions["bottom"] += 1

        return temp_collisions

    def check_player_collision(self, x, y) -> dict:
        # get all collisions based on the new position
        all_collisions = {'left': 0, 'bottom': 0, 'right': 0, 'top': 0}
        w, h = self.position[2], self.position[3]
        projected_player = pg.Rect.copy(self.position)
        projected_player.centerx = x
        projected_player.centery = y

        for rect in self.world.get_walls():
            if projected_player.colliderect(rect):
                pg.draw.rect(
                    self.screen, (255, 0, 0), pg.Rect(*rect.center, 10, 10)
                )
                
                new_collisions = self.check_collision(projected_player, rect)
                for dict_key in all_collisions.keys():
                    all_collisions[dict_key] += new_collisions[dict_key]

        return all_collisions

    def update_position(self) -> None:
        last_position = list(self.position.center)# position at last frame
    
        #new_position =  last_position
        self.collidict = dict.fromkeys(self.collidict, 0) # set all collision values to 0

        # update x position
        projected_x = self.position.centerx + round(self.x_momentum, 4)
        
        if self.DEBUG:
            print(self.position.centerx)
            print(projected_x)

        if self.x_momentum > 0:
            self.x_momentum -= abs(self.x_momentum / 2)

        elif abs(self.x_momentum) < 0.2:
            self.x_momentum = 0

        else:
            self.x_momentum += abs(self.x_momentum / 2)
        
        # check collision in horizontal direction
        x_collisions = self.check_player_collision(projected_x, self.position.centery) # checks if projected position is colliding with anything
        if sum(x_collisions.values()) > 0: # add only horizontal collisions to global dict
            self.collidict['left'] += x_collisions['left'] + x_collisions['bottom']
            self.collidict['right'] += x_collisions['right'] + x_collisions['top']

        # update y position
        projected_y = self.position.centery + round(self.y_momentum, 4)
        if self.y_momentum > 0:
            self.y_momentum -= abs(self.y_momentum / 2)

        elif abs(self.y_momentum) < 0.2:
            self.y_momentum = 0

        else:
            self.y_momentum += abs(self.y_momentum / 2)

        # check collision in vertical direction
        y_collisions = self.check_player_collision(self.position.centerx, projected_y) # check y projection for collision
        if sum(y_collisions.values()) > 0: # add only vertical collisions to global dict
            self.collidict['top'] += y_collisions['top'] + y_collisions['right']
            self.collidict['bottom'] += y_collisions['bottom'] + y_collisions['left']

        self.position.centerx, self.position.centery = projected_x, projected_y
        # change position if collision
        if sum(self.collidict.values()) > 0:
            if self.collidict["right"] or self.collidict["left"]:
                self.position.centerx = last_position[0]
            
            if self.collidict["top"] or self.collidict["bottom"]:
                self.position.centery = last_position[1]

        if self.DEBUG:
            print(self.collidict)

        # Update the screen
        self.summon()


    def _check_position(self, world) -> None:
        """Check if player is walking off a chunk"""

        changed, changey, changex = False, 0, 0
        if self.position.centerx <= 0:
            changex = -1
            self.position.centerx = self.screen.get_width()
            changed = True

        elif self.position.centerx >= self.screen.get_width():
            changex = 1
            self.position.centerx = 0
            changed = True

        elif self.position.centery <= 0:
            changey = 1
            self.position.centery = self.screen.get_height()
            changed = True

        elif self.position.centery > self.screen.get_height():
            changey = -1
            self.position.centery = 0
            changed = True

        if changed:
            # SAve the chunk the player just walked off of
            world.save_chunk()
            self.chunk[0] += changex
            self.chunk[1] += changey
            # blit new background
            world.update_background()

    # Function checks key_presses and handles accordingly
    def handle_keys(self, keys_pressed, world) -> None:
        # move player
        if keys_pressed[ord("w")]:
            self.y_momentum += -self.SPEED

        if keys_pressed[ord("s")]:
            self.y_momentum += self.SPEED

        if keys_pressed[ord("a")]:
            self.x_momentum += -self.SPEED

        if keys_pressed[ord("d")]:
            self.x_momentum += self.SPEED

        unchanged = self.slot_selected

        for slot in range(1, 4):
            if keys_pressed[ord(f"{slot}")]:
                self.slot_selected = slot

        # Only update if the taskbar is actually different
        if unchanged != self.slot_selected:
            self.update_taskbar()

        # Update player position
        self.update_position()

        self._check_position(world)

    # -------------- TASKBAR FUNCTIONS ------------#
    def update_taskbar(self) -> None:
        # Create taskbar. If the current index is the selected slot, border it.
        for i in range(1, 4):
            slot = self.FONTS[0].render(f"{i}", True, (0, 0, 0))
            if i == self.slot_selected:
                # Draw border
                pg.draw.rect(slot, (255, 255, 0), slot.get_rect(), 4)

            self.taskbar[i - 1] = slot

        # Create the font render for the taskbar msg
        try:
            key, val = list(self.stats["inv"].items())[self.slot_selected - 1]
            msg = f"{val} {key}"
        except IndexError:
            msg = "None"

        self.taskbar_msg = self.FONTS[1].render(msg, True, (255, 0, 0))
        self.world.save_chunk()
        self.world.save_char()

    # ----------------- ITEM FUNCTIONS --------------#
    def get_slot_item(self, slot):
        return list(self.stats["inv"].keys())[self.slot_selected - 1]

    def pick_item(self, item) -> None:
        # Check if the player has any of that item.
        # If they dont, add it and increase the count by one
        if item.NAME not in self.stats["inv"].keys():
            self.stats["inv"][item.NAME] = 0

        self.stats["inv"][item.NAME] += 1
        # Update the taskbar so that it shows up to date inventory info
        self.update_taskbar()
        print(f"Added {item.NAME} to inventory")

    def place_item(self, items) -> Dict:
        # Get the item in the selected slot
        if self.slot_selected <= len(self.stats["inv"]):
            # selected_item = list(self.stats["inv"].keys())[self.slot_selected -1]
            selected_item = self.get_slot_item(self.slot_selected)
            items = make_item(
                items, {selected_item: (1, self.position.center)}, self.screen
            )
            self.stats["inv"][selected_item] -= 1

            # If after placing the item, the amount of that item is 0, remove the item from inv
            if self.stats["inv"][selected_item] == 0:
                self.stats["inv"].pop(selected_item)
        self.update_taskbar()
        return items

    def use_item(self) -> None:
        """Use item in current slot"""
        # get inv item based on index
        try:  # TODO remove try except later
            item = list(self.stats["inv"].keys())[self.slot_selected - 1]
        except:
            item = ""
        # healing stuff
        if item == "Medkit":
            Medkit.special(self)
        self.update_taskbar()
